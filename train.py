from model import *
from data import *
import matplotlib.pyplot as plt
import numpy as np
from plot_keras_history import plot_history
from keras.callbacks import History
from tensorflow.keras.callbacks import EarlyStopping


data_gen_args = dict() # leave dict empty; no data augmentation used for training;
myGene = trainGenerator(2,'data/membrane/train','train','trainannot',data_gen_args,save_to_dir = None)
validGene = validGenerator(2,'data/membrane/train','val','valannot',save_to_dir = None)

early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=3)
reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=2, mode='min', min_lr=0.0001, verbose=1)
model = unet()
model_checkpoint = ModelCheckpoint('unet_membrane.hdf5', monitor='val_loss',verbose=1, save_best_only=True, mode='min')
history = model.fit_generator(myGene,steps_per_epoch=114,epochs=14, callbacks=[model_checkpoint, early_stopping], validation_data=validGene, validation_steps=32)

testGene = testGenerator("data/membrane/test")
results = model.predict_generator(testGene,72,verbose=1)
saveResult("data/membrane/test",results)


print(history.history.keys())

total_epochs_run=len(history.history['loss'])
total_epochs=total_epochs_run+1


loss_train = history.history['loss']
loss_val = history.history['val_loss']
epochs = range(1,total_epochs)
plt.plot(epochs, loss_train, 'g', label='Training loss')
plt.plot(epochs, loss_val, 'b', label='validation loss')
plt.title('Training loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.show()


acc_train = history.history['acc']
acc_val = history.history['val_acc']
epochs = range(1,total_epochs)
plt.plot(epochs, acc_train, 'g', label='Training accuracy')
plt.plot(epochs, acc_val, 'b', label='validation accuracy')
plt.title('Training and Validation accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.legend()
plt.show()

dice_train = history.history['f1-score']
dice_val = history.history['val_f1-score']
epochs = range(1,total_epochs)
plt.plot(epochs, dice_train, 'g', label='Training dice')
plt.plot(epochs, dice_val, 'b', label='validation dice')
plt.title('Training dice coeff')
plt.xlabel('Epochs')
plt.ylabel('Dice coeff')
plt.legend()
plt.show()

#
# hd_train = history.history['haus_dist']
# hd_val = history.history['val_haus_dist']
# epochs = range(1,total_epochs)
# plt.plot(epochs, dice_train, 'g', label='Training hd')
# plt.plot(epochs, dice_val, 'b', label='validation hd')
# plt.title('Training and Validation  hd')
# plt.xlabel('Epochs')
# plt.ylabel('HD')
# plt.legend()
# plt.show()
