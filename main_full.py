from data_preparation import *
from model import *
from data import *
import os
from natsort import natsorted
from thresholding import *
import argparse
import shutil

text = ("This is a program to segment temporalis muscle in 2D at the orbital"
        "level and calculate its cross sectional"
        "area using 3D brain MRI scans as input")
parser = argparse.ArgumentParser(description=text,
                                 epilog="Additional information:"
                                 "Input images must be 3D .nii"
                                 "files (not .nii.gz / DICOM or another format."
                                 "The folder containing your brain"
                                 "images ('images') must be in the"
                                 "same directory with all the scripts. You"
                                 "will be prompted to add the path to this"
                                 "main directory if running this"
                                 "code on the command line. If using a pbs"
                                 "job script/ running on hpc and"
                                 "not directly on the command line, choose"
                                 "the option 'hpc'(type 'python main_full.py"
                                 "-hpc')")
parser.add_argument('-hpc', '--cluster', action='store_true',
                    help="run programme using hpc/ not directly on"
                    "command line(pbs job script)", dest='hpc')
args = parser.parse_args()
print(args.hpc)

if args.hpc:
    path_main = os.getcwd()
##############################################################################
    path = path_main + "/images"
    slices_path = path_main + "/slices"  # path to slices folder
    path_to_test = path_main + "/data/MR/test"  # path to test folder for
    # eyeball segmentation
    path_to_temporalis_test = path_main + "/temporalis_model/data/MR/test"
    path_to_normalized = path_main + "/slices/normalized"
    normalization = ('zscore-normalize' + " " + '-i' + " " + slices_path +
                     " " + '-o' + " " + path_to_normalized + " " + '-v')
    number_of_total_images = len(os.listdir(path))

else:
    # main directory where all the scripts are:
    path_main = os.getcwd()
    # path to image folder:
    path = path_main + "/images"
    # path to folder with sliced images:
    slices_path = path_main + "/slices"
    # path to test folder for eyeball
    # predictions:
    path_to_test = path_main + "/data/MR/test"
    # path to temporalis slices and segmentations:
    path_to_temporalis_test = path_main + "/temporalis_model/data/MR/test"
    # path to normalized images:
    path_to_normalized = path_main + "/slices/normalized"
    normalization = ('zscore-normalize' + " " + '-i' + " " + slices_path +
                     " " + '-o' + " " + path_to_normalized + " " + '-v')

    number_of_total_images = len(os.listdir(path))
    print('YOUR NUMBER OF IMAGES IS', number_of_total_images)

###############################################################################

# function to remove folders once they are not longer used or needed
def remove_folder(path):
    if os.path.isdir(path):
        shutil.rmtree(path)  # remove dir and all contains
    else:
        raise ValueError("file {} is not a dir.".format(path))

#                             DATA PREPARATION

# renaming images
rename(path, path_main)
print('Slicing images into 2D slices...')
# slicing images
slicing(path, slices_path)
os.chdir(path_main)
os.chdir(slices_path)
# creating a directory for normalized images
if not os.path.exists('normalized'):
        os.makedirs('normalized')
print('Preprocessing images....')
# normalizing and preprocessing images
preprocessing(path_to_normalized, normalization)
# converting to PNG and moving to test folder for segmentation
converting(path_to_normalized, path_to_test)

# REMOVING NORMALIZED AND PREPROCESSED SLICES WHICH WILL NO LONGER BE NEEDED,
# IF WANT TO KEEP - COMMENT OUT THIS LINE:
remove_folder(slices_path)



#                        EYEBALL SEGMENTATION
os.chdir(path_main)
number_of_test_images = len(os.listdir(path_to_test))
# Loading model and test data
data_gen_args = dict()
model = unet()
testGene = testGenerator("data/MR/test", path_to_test)

# Creating a list to rename the resulting predicted files
names = []
for filename in natsorted(os.listdir(path_to_test)):
    if filename.endswith('.png'):
        total = len(filename) - 4
        finalname = 'predict' + filename[0:total] + '.png'
        names.append(finalname)

# Loading weights for pre-trained model
model.load_weights("unet_eyeball.hdf5")
print('Segmenting eyeball...')

# Using U-Net to produce segmentations of the eyeball
results = model.predict_generator(testGene, number_of_test_images, verbose=1)

saveResult(path_to_test, results, names)

#                  SELECTING CORRECT SLICE
os.chdir(path_main)

if not os.path.exists('temporalis_model/data/MR/test'):
        os.makedirs('temporalis_model/data/MR/test')

print('Selecting the correct slices....')
selecting_slices(number_of_total_images, path_to_test, path_to_temporalis_test)


#                 TEMPORALIS SEGMENTATION
os.chdir(path_main)
# getting total number of images
number_of_test_images_temporalis = len(os.listdir(path_to_temporalis_test))
#  loading model and test images
data_gen_args = dict()
model = unet()
testGene = testGenerator("temporalis_model/data/MR/test",
                         path_to_temporalis_test)

# Creating a list to rename the resulting predicted files in
# the format "predict_patient_number_slice_number.png"
names_temp = []
for filename in natsorted(os.listdir(path_to_temporalis_test)):
    if filename.endswith('.png'):
        # total = len(filename)-4
        # finalname = 'predict' + filename[0:total] + '.png'
        finalname = 'predict' + filename
        names_temp.append(finalname)

model.load_weights("unet_temporalis.hdf5")
print('Segmenting temporalis...')
results = model.predict_generator(testGene, number_of_test_images_temporalis,
                                  verbose=1)
saveResult(path_to_temporalis_test, results, names_temp)


# TEMPORALIS CSA CALCULATION
temporalis_csa(path_to_temporalis_test, path_main)
