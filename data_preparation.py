import numpy as np
import nibabel as nib
import imageio
import os
from natsort import natsorted
import pandas as pd
import ants

# Renaming images to have numbers from 0 to total nr of images and saving to a csv file the orginal name and the corresponding new image name

# Creating the directory where preprocessed slices will be saved
if not os.path.exists('data/MR/test'):
    os.makedirs('data/MR/test')


# Renaming files and saving original and new names in an excel sheet
def rename(path_to_images, main_path):
 i=0
 new_names=[]
 original_names=[]
 os.chdir(path_to_images)
 for filename in natsorted(os.listdir(path_to_images)):
    if filename.endswith('.nii'):
      original_names.append(filename)
      os.rename(filename, str(i)+'.nii')
      renamed='%d.nii'%i
      new_names.append(renamed)
      i = i + 1

 os.chdir(main_path)
 n={'Original file names': original_names, 'New_names': new_names}
 names=pd.DataFrame(n)
 names.to_csv('data_dictionary.csv')


################### Slicing 3D images into 2D slices ######################

# Creating a folder to save the slices in
if not os.path.exists('slices'):
    os.makedirs('slices')

def slicing (path_to_images, path_to_slices_folder):
  os.chdir(path_to_images)
  i=0
  for filename in natsorted(os.listdir(path_to_images)):
   img=nib.load(filename)
   # print(filename)
   im1=img.get_fdata() #creating a numpy array
   print(im1.shape)
   slices=im1.shape[2]
   for n in range(0, slices):
    image=np.rot90(im1[:, :,n]) #rotates image 90 degrees to the left, if this is not needed you can skip this step and run the code below, changing image to im[:,:,n]
    imageio.imwrite(os.path.join(path_to_slices_folder,'%d_'%i +str(n)+'.nii'), image)
   i=i+1



###################### Preprocessing slices ######################

def preprocessing(path_to_normalized, normalization_querry):
#performing z-score normalization, bias-field correction, resizing and resampling images
 os.system(normalization_querry) 
 os.chdir(path_to_normalized)
 for filename in natsorted(os.listdir(path_to_normalized)):
    if filename.endswith('zscore.nii.gz'):
     print(filename)
     a=filename
     img=nib.Nifti1Image.load(filename).get_fdata()
     image_1= ants.from_numpy(img, origin=(0,0))
     image_1_n4 = ants.n4_bias_field_correction(image_1)
     img_res = ants.resample_image(image_1_n4, (256,256), True, 1)
     img_res.set_spacing((1,1)) #set voxel spacing as it was different after resizing for some images
    # print('spacing', img_res.spacing, 'origin', img_res.origin)
     name='preprocessed_' + str(a)
     ants.image_write(img_res, name)


############## Renaming, converting to png and transfering preprocessed slices to the test folder ##################

def converting(path_to_normalized, path_to_test_folder):
 for filename in natsorted(os.listdir(path_to_normalized)):
  if filename.startswith('preprocessed_'):
    img=nib.load(filename)
    im1=img.get_fdata()
    image=np.rot90(np.rot90(np.rot90(im1[:,:])))
    total=len(filename)-14 # for the final name selecting the part of the filename without preprocessing and without .nii.gz the preprocessed files are labelled like 'preprocessed_patientnumber_slicenumber_zscore.nii.gz'
    name=filename[13:total]+'.png'
    os.chdir(path_to_test_folder)
    imageio.imwrite(name, image[:,:])
    os.chdir(path_to_normalized)
